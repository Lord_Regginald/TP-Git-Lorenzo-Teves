class Juego
{
    protected $ganador = '';
    protected $rondas = 0;

    public function jugar(Adivinador $jugador1, Adivinador $jugador2, Adivinador $jugador3, Adivinador $jugador4, Adivinador $jugador5, Adivinador $jugador6)
    {
        $this->rondas = 0;
        $this->ganador = '';
        $numSecreto = rand(1, 1000);
        while ($this->ganador == '') {
            $num1 = $jugador1->sugerirNumeroSecreto();
            $num2 = $jugador2->sugerirNumeroSecreto();
            $num3 = $jugador1->sugerirNumeroSecreto();
            $num4 = $jugador2->sugerirNumeroSecreto();
            $num5 = $jugador1->sugerirNumeroSecreto();
            $num6 = $jugador2->sugerirNumeroSecreto();

            if ($this->hayGanador($numSecreto, $num1, $num2) == false) {
                $this->informarDesvio($jugador1, $num1, $numSecreto);
                $this->informarDesvio($jugador2, $num2, $numSecreto);
            }
            $this->rondas++;
        }
        return $this->ganador;
    }
    protected function hayGanador($numSecreto, $num1, $num2, $num3, $num4, $num5, $num6)
    {
        if ($num1 == $numSecreto || $num2 == $numSecreto || $num3 == $numSecreto || $num4 == $numSecreto || $num5 == $numSecreto || $num6 == $numSecreto) {
            if ($num1 == $num2 == $num3 == $num4 == $ num5 == $num6) {
                $this->$ganador = 'Todos ganan, yay!';
            } elseif ($num1 == $numSecreto) {
                $this->$ganador = 'Ganó el jugador 1.';
            } elseif ($num2 == $numSecreto) {
                $this->$ganador = 'Ganó el jugador 2.';
            } elseif ($num3 == $numSecreto) {
                $this->$ganador = 'Ganó el jugador 3.';
            } elseif ($num4 == $numSecreto) {
                $this->$ganador = 'Ganó el jugador 4.';
            } elseif ($num5 == $numSecreto) {
                $this->$ganador = 'Ganó el jugador 5.';
            } else {
                $this->$ganador = 'Ganó el jugador 6.';
            }
            return true; // Alguno de los jugadores acertó, devolvemos true.
        }
        return false; // Ningún jugador acertó, devolvemos false.
    }
    protected function informarDesvio(Adivinador $jugador, $numero, $numeroSecreto)
    {
        if ($numero > $numeroSecreto) {
            $jugador->elNumeroEraMayor();
            else {
                $jugador->elNumeroEraMenor();
            }
        }
    }
}
$juego = new Juego;
$gano1 = $gano2 = 0;
for ($i = 1; $i < 10; $i++) {
    $jugador1 = new Profesional;
    $jugador2 = new Principiante;
    $jugador3 = new Principiante;
    $jugador4 = new Principiante;
    $jugador5 = new Principiante;
    $jugador6 = new Profesional;
    $resultadoJuego = $juego->jugar($jugador1, $jugador2, $jugador3, $jugador4, $jugador5, $jugador6);

    if ($resultadoJuego == 'Ganó el jugador 1.') {
        $gano1++;
    } elseif ($resultadoJuego == 'Ganó el jugador 2.') {
        $gano2++;
    } elseif ($resultadoJuego == 'Ganó el jugador 3.') {
        $gano3++;
    } elseif ($resultadoJuego == 'Ganó el jugador 4.') {
        $gano4++;
    } elseif ($resultadoJuego == 'Ganó el jugador 5.') {
        $gano5++;
    } elseif ($resultadoJuego == 'Ganó el jugador 6.') {
        $gano6++;
    }
    echo $resultadoJuego;
}
if ($gano1 == $gano2 == $gano3 == $gano4 == $gano5 == $gano6) {
    echo "Hubo un empate en el torneo.";
} elseif ($gano1 > $gano2 && $gano1 > $gano3 && $gano1 > $gano4 && $gano1 > $gano5 && $gano1 > $gano6) {
    echo "Ganó el torneo el jugador 1.";
} elseif ($gano2 > $gano1 && $gano2 > $gano3 && $gano2 > $gano4 && $gano2 > $gano5 && $gano2 > $gano6) {
    echo "Ganó el torneo el jugador 2.";
} elseif ($gano2 > $gano1 && $gano2 > $gano3 && $gano2 > $gano4 && $gano2 > $gano5 && $gano2 > $gano6) {
    echo "Ganó el torneo el jugador 3.";
} elseif ($gano2 > $gano1 && $gano2 > $gano3 && $gano2 > $gano4 && $gano2 > $gano5 && $gano2 > $gano6) {
    echo "Ganó el torneo el jugador 4.";
} elseif ($gano2 > $gano1 && $gano2 > $gano3 && $gano2 > $gano4 && $gano2 > $gano5 && $gano2 > $gano6) {
    echo "Ganó el torneo el jugador 5.";
} else {
    echo "Ganó el torneo el jugador 6.";
}
